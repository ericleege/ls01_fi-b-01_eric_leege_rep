package package_schiff;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Die Klasse Schiff deklariert die Konstruktoren sowohl alle ben�tigten Methoden 
 * @author Eric
 */
public class Schiff {
    int photonentorpedoAnzahl;
    int energieversorgungInProzent;
    int schildeInProzent;
    int huelleInProzent;
    int lebenserhaltungssystemeInProzent;
    String schiffsname;
    ArrayList<Cargo> ladungsverzeichnis = new ArrayList();	

    /**
     * Das ist der Konstruktor f�r das Raumschiff  
     *   
     * @param schiffsname deklariert den Schiffsnamen
     * @param energieversorgungInProzent zeigt die Energieversorgung in Prozent an
     * @param photonentorpedoAnzahl definiert die Anzahl der Photonentorpedos
     * @param schildeInProzent zeigt die Schildst�rke in Prozent an
     * @param lebenserhaltungssystemeInProzent deklariert die LEbenserhaltungssystem in Prozent an
     * @param huelleInProzent gibt die St�rke der H�lle in Prozent an
     */
    public Schiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
    	this.schiffsname = schiffsname;
    	this.energieversorgungInProzent = energieversorgungInProzent;
    	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.schildeInProzent = schildeInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.huelleInProzent = huelleInProzent;
    }
    /**
     *	Hier wird der derzeitige Zustand mit allen Variablen ausgegeben
     */
    public void zustand() {
    	System.out.println("");
        System.out.println("Derzeitiger Zustand der: " + this.getSchiffsname());
        System.out.println("Photonentorpedo: " + this.getPhotonentorpedoAnzahl());
        System.out.println( "Energieversorgung: " + this.getEnergieversorgungInProzent() );
        System.out.println( "Schild: " + this.getSchildeInProzent() );
        System.out.println( "Huelle: "+ this.getHuelleInProzent() );
        System.out.println("Lebenserhaltungssysteme: "+ this.getLebenserhaltungssystemeInProzent() );
    }
/**
 *     	Die Ladung wird erweitert
 * 		@param ladung gibt an was hinzugef�gt wird
 */
    public void addLadung(Cargo ladung) {
        this.ladungsverzeichnis.add(ladung);
    }
/**
 * 	Hier wird die Ladung der Schiffe ausgegeben, die while Schleife gibt die Ladungen aus mit Anzahl
 */
    public void ladungsverzeichnisAusgeben() {
        System.out.println("");
    	System.out.println("Ladungsverzeichnis der " + this.getSchiffsname() + ":");
        Iterator p = this.ladungsverzeichnis.iterator();
        while(p.hasNext()) {
        	Cargo  ladungsverzeichnis = (Cargo )p.next();
            if (ladungsverzeichnis.getBezeichner().length() <= 15) {
                System.out.println("Bezeichner: "+ ladungsverzeichnis.getBezeichner());
            } else {
                System.out.println("Bezeichner: "+ ladungsverzeichnis.getBezeichner());
            }
            System.out.println("Menge: "+ ladungsverzeichnis.getMenge());
        }
    }
/**
 *  Die Kanone wird hier abgefeuert
 * 	@param h definiert das Ziel welches angegriffen werden soll
 */
    public void phaserkanoneSchiessen(Schiff h) {
        if (this.getEnergieversorgungInProzent() >= 50) {
            this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent() - 50);
            this.treffer(h);
    }
    }
/**
 *  Hier werden die Torpedos abgeschossen
 * 	@param h definiert das Ziel welches angegriffen werden soll
 */
    public void photonentorpedosSchiessen(Schiff h) {
        if (this.getPhotonentorpedoAnzahl() != 0) {
            this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() - 1);
            this.treffer(h);
        } 
    }
/**
 * 	In dem Teil wird ein Treffer im System vermerkt und es erfolgt eine Benachrichtigung
 *  @param h definiert welches Ziel getroffen worden ist
 */
    public void treffer(Schiff h) {
        System.out.println(h.getSchiffsname() + " wurde getroffen!");
        if (h.getHuelleInProzent() <= 0) {
            h.setHuelleInProzent(0);
        } else if (h.getSchildeInProzent() <= 0) {
            h.setHuelleInProzent(h.getHuelleInProzent() - 50);
            h.setEnergieversorgungInProzent(h.getEnergieversorgungInProzent() - 50);
        } else {
            h.setSchildeInProzent(h.getSchildeInProzent() - 50);
            if (h.getSchildeInProzent() <= 0) {
                h.setSchildeInProzent(0);
            }
        }
    }
//    Hier werden die einzelnen Attribute gepr�ft und Ausgaben get�tigt nach Abfrage
    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }
    public int getPhotonentorpedoAnzahl() {
        return this.photonentorpedoAnzahl;
    }
    
    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }
    public int getEnergieversorgungInProzent() {
        return this.energieversorgungInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }
    public int getSchildeInProzent() {
        return this.schildeInProzent;
    }


    
    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }
    public int getHuelleInProzent() {
        return this.huelleInProzent;
    }
    


    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }
    public String getSchiffsname() {
        return this.schiffsname;
    }


    public int getLebenserhaltungssystemeInProzent() {
        return this.lebenserhaltungssystemeInProzent;
    }
}

