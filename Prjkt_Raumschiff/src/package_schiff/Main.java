package package_schiff;

public class Main {

    public static void main(String[] args) {
    	
        Schiff s_1 = new Schiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
        Schiff s_2 = new Schiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
        Schiff s_3 = new Schiff(0, 80, 80, 50, 100, 5, "Ni'Var");
        Cargo s1_c1 = new Cargo ("Ferengi Schneckensaft", 200);
        Cargo s1_c2 = new Cargo ("Bat'leth Klingonen Schwert", 200);
        Cargo s2_c1 = new Cargo ("Borg-Schrott", 5);
        Cargo s2_c2 = new Cargo ("Rote Materie", 2);
        Cargo s2_c3 = new Cargo ("Plasma-Waffe", 50);
        Cargo s3_c1 = new Cargo ("Forschungssonde", 35);
        Cargo s3_c2 = new Cargo ("Photonentorpedo", 3);
       
        s_1.addLadung(s1_c1);
        s_1.zustand();
        s_1.addLadung(s1_c2);
        s_1.zustand();
        s_1.photonentorpedosSchiessen(s_2);
        s_1.photonentorpedosSchiessen(s_2);
        s_1.photonentorpedosSchiessen(s_2);
        s_1.ladungsverzeichnisAusgeben();
        s_1.zustand();
        
        s_2.addLadung(s2_c1);
        s_2.ladungsverzeichnisAusgeben();
        s_2.zustand();
        s_2.ladungsverzeichnisAusgeben();
        s_2.addLadung(s2_c2);
        s_2.phaserkanoneSchiessen(s_1);
        s_2.zustand();
        s_2.addLadung(s2_c3);
        
        s_3.addLadung(s3_c1);
        s_3.zustand();
        s_3.addLadung(s3_c2);
        s_3.zustand();
        s_3.ladungsverzeichnisAusgeben();
    }
}

