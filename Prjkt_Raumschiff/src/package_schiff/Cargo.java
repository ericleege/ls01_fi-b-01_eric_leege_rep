package package_schiff;

public class Cargo {
    String bezeichner;
    int menge;

    public Cargo(String bezeichner, int menge) {
        this.bezeichner = bezeichner;
        this.menge = menge;
    }
    public void setMenge(int menge) {
        this.menge = menge;
    }
    public int getMenge() {
        return this.menge;
    }
    public void setBezeichner(String bezeichner) {
        this.bezeichner = bezeichner;
    }
    public String getBezeichner() {
        return this.bezeichner;
    }
}

